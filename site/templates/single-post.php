<?php include 'header.php'; ?>
    <section class="post">
    <h1><?php echo $page->title; ?></h1>
    <div><?php echo $page->body; ?></div>

    <?php
    $images = $page->images;
    if (count($images)) {
      echo "<h2>Image(s) jointe(s) :</h2>";
      foreach ($images as $image) {
        echo "<img src='$image->url'>";
      }
    }
     ?></section>
     <div>
     <?php
     $files = $page->attachment;
     if (count($files)) {
       echo "<h2>Pièce(s) jointe(s):</h2>";
       echo "<ul>";
       foreach ($files as $file) {
         echo "<li><a href='$file->url'>Fichier joint</a></li>";
       }
       echo "</ul>";
     }
      ?>
    </div>
<?php include 'footer.php'; ?>
