<?php include 'header.php'; ?>
    <div class="list">
    <?php
    $posts = $pages->find("template=single-post, sort=date");
    $c = false;
    foreach ($posts as $post){
      echo "<a class='post' href='{$post->url}'>";
      echo "<h2>$post->title</h2>";
      if (count($post->images)) {
        $image = $post->images->first();
        $thumb = $image->size(100, auto);
        echo "<img src='$thumb->url'>";
      }elseif (count($files)) {
        foreach ($post->attachment as $file) {
          if ($file->ext=="png" && $c == false ) {
            echo "<img  class='attachment' src='$file->url'>";
            $c = !c;
          }
        }
      }
      echo "</a>";
    }
    ?>
    </div>
<?php include 'footer.php'; ?>
