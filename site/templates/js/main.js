var toggleOrder = document.getElementsByClassName('toggleOrder');
var body = document.body;
for (var i = 0; i < toggleOrder.length; i++) {
  toggleOrder[i].addEventListener('click',function(){
    body.classList.toggle('lastfirst');
  });
}

$('.post').each(function(){
  var that = $(this);
  var text = that.find('.content').html();
  var words = text.split(' ');
  var rt = that.find('.readingtime');
  var time = that.find('.clock').attr('data-time');
  setClock(that, time);
  var author = that.find('.author').data();
  rt.html(secondsToHms(Math.floor(words.length / 2.8)));
  txt = that.html().replace('--', '<div class="signature">').replace("<hr>", "</div>")
  that.html(txt);
  console.log(text);


})

// READING TIME

function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? "h " : "h, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? "m " : "m, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? "s" : "s") : "";
    return hDisplay + mDisplay + sDisplay;
}

// clock

function setClock(el, time){
  var hands = [];
  var min = el.find('.minutehand')
  var hour = el.find('.hourhand')
  // hands.push(document.querySelector('#secondhand > *'));
  hands.push(min);
  hands.push(hour);

  var cx = 100;
  var cy = 100;

  function shifter(val) {
    return [val, cx, cy].join(' ');
  }
  var date = new Date(time*1000);

  var hoursAngle = 360 * date.getHours() / 12 + date.getMinutes() / 2;
  var minuteAngle = 360 * date.getMinutes() / 60;
  // var secAngle = 360 * date.getSeconds() / 60;

  // document.querySelector('#secondhand').setAttribute('transform', 'rotate('+shifter(secAngle)+')');
  min.attr('transform', 'rotate('+shifter(minuteAngle)+')');
  hour.attr('transform', 'rotate('+shifter(hoursAngle)+')');

  // for(var i = 1; i <= 12; i++) {
  //   var el = document.createElementNS('http://www.w3.org/2000/svg', 'line');
  //   el.setAttribute('x1', '100');
  //   el.setAttribute('y1', '30');
  //   el.setAttribute('x2', '100');
  //   el.setAttribute('y2', '40');
  //   el.setAttribute('transform', 'rotate(' + (i*360/12) + ' 100 100)');
  //   el.setAttribute('style', 'stroke: #ffffff;');
  //   document.querySelector('svg').appendChild(el);
  // }
  //
}
