<?php include 'header.php'; ?>
    <div class="book">
      <a class='button reload' target='_blank' href='<?php echo $hashURL ?>'>⟲</a>
    <?php
    $posts = $pages->find("template=single-post, sort=emaildate");
    foreach ($posts as $post){

      $title = $post->title;
      $body = $post->body;
      $sep = $post->separateur;
      // $body = strip_tags($body, '<b><i><a><hr><br><table><img><ul><li><ol>]');
      // $body = htmlspecialchars_decode($body);
      $sender = $post->sender;
      $files = $post->attachment;
      $date = $post->emaildate;
      $cooldate = date('d/m/Y',$date);
      if ($sep) {
        echo "<section class='post separateur'>";
      }else {
        echo "<section class='post'>";
      }
      echo "<header>";
      echo "<a class='button edit' href='$post->editURL'>Modifier le post</a>";
      echo "<h1>$post->title</h1>";
      echo "<div class='widget-list'>";
      echo "<div class='widget avatar'>$sender</div>";
      echo "<div class='widget clock' data-time='$date'>";
      include 'clock.php';
      echo "</div>";
      echo "<div class='widget meteo'>$date</div>";
      echo "<div class='widget readingtime'>$count</div>";
      echo "<div class='widget version'>$config->doncversion</div>";
      echo "</div>"; // end widgets
      echo "</header>";
      echo "<div class='row'>";
      echo "<div class='meta'>";
      echo "<div class='date'>";
      echo $cooldate;
      echo "</div>";
      echo "<div class='author'>";
      echo $sender;
      echo "</div>";
      echo "</div>"; // end meta
      echo "<div class='content'>$body";
      if (count($post->images)) {
        foreach ($post->images as $image) {
          $thumb = $image->size(800, auto);
          echo "<img src='$thumb->url'>";
        }
      }
      if (count($files)) {
        foreach ($files as $file) {
          if ($file->ext==='jpg' || $file->ext==='png' || $file->ext==='gif' || $file->ext==='jpeg') {
            echo "<img src='$file->url'>";
          }
        }
      }

      echo "</div>";
      echo "</div>"; // end row
      echo "</section>";
    }
    ?>
    </div>
<?php include 'footer.php'; ?>
