<?php

class EmailImageConfig extends \ProcessWire\Wire {

	public function getConfig(array $data) {

		// check that they have the required PW version
		if(version_compare(\ProcessWire\wire('config')->version, '2.2.13', '<')) {
			$this->error("Email Image requires ProcessWire 2.2.13 or newer. Please update.");
		}

		$modules = \ProcessWire\wire('modules');
		$form = new \ProcessWire\InputfieldWrapper();

		$field = $modules->get("InputfieldText"); 
		$field->attr('name', 'pop3_hostname'); 
		$field->attr('value', $data['pop3_hostname']); 
		$field->label = \ProcessWire\__('POP3 hostname');
		$field->columnWidth = 50;
		$field->required = true; 
		$form->add($field); 

		$field = $modules->get("InputfieldInteger"); 
		$field->attr('name', 'pop3_port'); 
		$field->attr('value', $data['pop3_port']); 
		$field->label = \ProcessWire\__('POP3 port');
		$field->columnWidth = 20;
		$field->required = true; 
		$form->add($field); 

		$field = $modules->get("InputfieldInteger"); 
		$field->attr('name', 'wait_seconds'); 
		$field->attr('value', $data['wait_seconds']); 
		$field->label = \ProcessWire\__('Check every # seconds');
		$field->columnWidth = 30;
		$field->required = true; 
		$form->add($field); 

		$field = $modules->get("InputfieldText"); 
		$field->attr('name', 'pop3_user'); 
		$field->attr('value', $data['pop3_user']); 
		$field->label = \ProcessWire\__('POP3 user');
		$field->columnWidth = 50;
		$field->required = true; 
		$form->add($field); 

		$field = $modules->get("InputfieldText"); 
		$field->attr('name', 'pop3_password'); 
		$field->attr('value', $data['pop3_password']); 
		$field->attr('type', 'password'); 
		$field->label = \ProcessWire\__('POP3 password');
		$field->columnWidth = 50;
		$field->required = true; 
		$form->add($field); 

		$field = $modules->get("InputfieldTextarea"); 
		$field->attr('name', 'valid_senders'); 
		$field->attr('value', $data['valid_senders']); 
		$field->label = \ProcessWire\__('Valid senders');
		$field->description = \ProcessWire\__('Enter a list of email addresses (1 per line) to process emails from.'); 
		$form->add($field); 

		$fieldset = $modules->get('InputfieldFieldset'); 
		$fieldset->label = \ProcessWire\__('Advanced'); 
		$fieldset->attr('name', '_advanced'); 
		$fieldset->collapsed = \ProcessWire\Inputfield::collapsedYes; 

		$field = $modules->get('InputfieldCheckbox'); 
		$field->attr('name', 'pop3_apop'); 
		$field->attr('value', 1); 
		$field->attr('checked', $data['pop3_apop'] ? 'checked' : ''); 
		$field->columnWidth = 50;
		$field->label = \ProcessWire\__('Use APOP?'); 
		$field->notes = \ProcessWire\__('In rare cases this may be required.'); 
		$fieldset->add($field); 

		$field = $modules->get('InputfieldCheckbox'); 
		$field->attr('name', 'pop3_tls'); 
		$field->attr('value', 1); 
		$field->attr('checked', $data['pop3_tls'] ? 'checked' : ''); 
		$field->columnWidth = 50;
		$field->label = \ProcessWire\__('Use TLS?'); 
		$field->notes = \ProcessWire\__('GMail uses TLS and port 993.'); 
		$fieldset->add($field); 

		$field = $modules->get("InputfieldText"); 
		$field->attr('name', 'pop3_body_txt_start'); 
		$field->attr('value', $data['pop3_body_txt_start']); 
		$field->label = \ProcessWire\__('Tag or word that starts email body text for image description');
		$field->notes = \ProcessWire\__('Example: [text]'); 
		$field->columnWidth = 50;
		$fieldset->add($field); 

		$field = $modules->get("InputfieldText"); 
		$field->attr('name', 'pop3_body_txt_end'); 
		$field->attr('value', $data['pop3_body_txt_end']); 
		$field->label = \ProcessWire\__('Tag or word that ends email body text for image description');
		$field->notes = \ProcessWire\__('Example: [/text]'); 
		$field->columnWidth = 50;
		$fieldset->add($field); 

		$form->add($fieldset); 

		$field = $modules->get('InputfieldCheckbox');
		$field->attr('name', '_test_settings');
		$field->label = \ProcessWire\__('Test settings now');
		$field->attr('value', 1);
		$field->attr('checked', '');
		$form->add($field); 

		if(\ProcessWire\wire('session')->test_settings) {
			\ProcessWire\wire('session')->remove('test_settings'); 
			$field->notes = $this->testSettings();

		} else if(\ProcessWire\wire('input')->post->_test_settings) {
			\ProcessWire\wire('session')->set('test_settings', 1); 
		}

		$file = \ProcessWire\wire('config')->paths->templates . 'email-images.php';
		if(!is_file($file)) $this->error("Please copy the file /site/modules/EmailImage/email-images.php to /site/templates/email-images.php"); 

		return $form;
	}

	public function testSettings() {

		$errors = array();
		$success = false; 
		$module = \ProcessWire\wire('modules')->get('EmailImage');

		try {
			$a = $module->getAdaptor();
			$success = $a->testConnection();
			
		} catch(Exception $e) {
			$errors[] = $e->getMessage(); 
		}

		if($success) {
			$note = $this->_('SUCCESS! Email settings appear to work correctly.');
			$this->message($note); 
			$url = \ProcessWire\wire('pages')->get("template=email-images")->url; 
			$this->message("Go ahead and email yourself an image or two, wait a minute (or two), and then view <a href='$url'>this page</a>."); 

		} else {
			$note = $this->_('ERROR: email settings did not work.'); 	
			$this->error($note);
			foreach($a->getErrors() as $error) $this->error($error); 
		}

		return $note; 	
	}
}
