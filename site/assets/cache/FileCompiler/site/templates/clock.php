<svg width="200" height="200">
    <g>
        <circle id="circle" style="stroke: #000; stroke-width: 1px; fill:#fff" cx="100" cy="100" r="80"></circle>
    </g>
    <g>
        <line x1="100" y1="100" x2="100" y2="65" transform="rotate(80 100 100)" style="stroke-width: 1px; stroke: #000;" class="hourhand"></line>
        <line x1="100" y1="100" x2="100" y2="40" style="stroke-width: 5px; stroke: #000;" class="minutehand"></line>
        <line x1="100" y1="100" x2="100" y2="20" style="stroke-width: 0px; stroke: #000;" class="secondhand"></line>
    </g>
    <circle id="center" style="fill:#000; stroke: #000; stroke-width: 1px;" cx="100" cy="100" r="3"></circle>
</svg>
