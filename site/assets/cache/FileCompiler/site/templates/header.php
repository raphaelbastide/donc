<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>DONC - <?php echo $page->title; ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>fonts/stylesheet.css" />
    <?php if ($page->template == "book"): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>css/print.css" />
    <?php else: ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>css/main.css" />
    <?php endif; ?>
  </head>
  <?php
  echo "<body class='$page->template'>";
   ?>
    <header>
      <?php
      $base = \ProcessWire\wire('pages')->get('/')->httpUrl;
      $hashURL = $base."?hash=".$config->hash;
      // $hashRUL = $config->hashURL;
      ?>

      <h1><a href="<?php echo $base ?>">DONC</a></h1>
      <?php
      if ($page->template != "book") {
        echo "<p><a class='button' href='".$base."book'>⋎</a></p>";
      }
       ?>
      <p><a class='button' target='_blank' href='<?php echo $hashURL ?>'>⟲</a></p>
      <?php
      if($page->editable() && $page->title != "Home" && $page->template != "book")  echo "<p><a class='button' href='$page->editURL'>✎</a></p>";
      if ($page->title == "Home" && $page->template != "book" ) {
        echo "<p class='toggleOrder button'>↑↓</p>";
      }
      ?>
    </header>
